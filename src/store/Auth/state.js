export default function () {
  return {
    user: null,
    userFullname: localStorage.getItem('userFullname'),
    token: localStorage.getItem('token') || '',
  }
}
