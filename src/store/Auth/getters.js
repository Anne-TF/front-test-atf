export function getToken (state) {
    return !!state.token;
}

export function getUser (state) {
    return state.user;
}

export function getUserFullname (state) {
    return state.userFullname ? state.userFullname : ''
}
