import { api } from 'boot/axios';

export function login ({commit}, payload) {
    return api
            .post('/api/auth/login?provider=local', payload)
            .then(res => {
                if(res.data && res.data.code === 201) {
                    const { user } = res.data.data
                    const token = `Bearer ${res.data.data.token}`
                    localStorage.setItem('token', token)
                    localStorage.setItem('userFullname', user.firstName.concat(' ', user.lastName))
                    commit('setUserAndToken', res.data.data)
                    api.defaults.headers.common['Authorization'] = token
                }
                return res
            })
            .catch(err => {
                console.log('>> Error while requesting to /api/auth/login in /store/auth/actions.', err)
                return err
            })
  }

  export function logout ({commit}) {
    localStorage.removeItem('token')
    localStorage.removeItem('userFullname')
    delete api.defaults.headers.common['Authorization']
    commit('logout')
  }
