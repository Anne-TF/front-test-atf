export function setUserAndToken (state, payload) {
    state.user = payload.user
    state.token = payload.token
    state.userFullname = payload.user.firstName.concat(' ', payload.user.lastName)
}

export function logout (state) {
    state.user = null
    state.token = ''
    state.userFullname = localStorage.getItem('userFullname')
}
